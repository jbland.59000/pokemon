export default interface Type {
    _type: string;
    _faiblesse: string[];
    _resistance: string[];

    get type(): string;
    getModifier(type: string): number;
    addFaiblesse(...faiblesses: string[]): void;
    addResistance(...resistances: string[]): void;
    removeFaiblesse(...faiblesses: string[]): void;
    removeResistance(...resistances: string[]): void;
}