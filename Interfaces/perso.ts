export default interface Perso {
    _nom: string;

    renommer(nom: string): void;
    get nom(): string;
}