import Pokemon from "../classes/pokemon";

export default interface Combat{
    _puissance: number;
    _vitalite: number;
    _vitaliteMax: number;

    attaque(pokemon: Pokemon): boolean;
    subirDegats(total: number): void;
    get KO();
}