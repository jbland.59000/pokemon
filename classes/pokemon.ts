import Combat from "../Interfaces/combat";
import Perso from "../Interfaces/perso";
import Type from "../Interfaces/type";

export default abstract class Pokemon implements Combat, Perso, Type  {
    _puissance: number;
    _vitalite: number;
    _vitaliteMax: number;
    _type: string;
    _faiblesse: string[];
    _resistance: string[];
    _nom: string;
    _niveau: number;

    constructor(nom: string, niveau: number){
        this._nom = nom;
        this.monterNiveau(niveau);
        this._vitalite = this._vitaliteMax;
    }
    abstract monterNiveau(total?: number): void;

    renommer(nom: string){
        this._nom = nom;
    }
    attaque(pokemon: Pokemon): boolean {
        let degats = this._puissance * pokemon.getModifier(this.type);
        return pokemon.subirDegats( this._puissance);
    }
    subirDegats(total: number): boolean {
        this._vitalite -= total;
        return this.KO;
    }
    getModifier(type: string): number {
        if( this._faiblesse.indexOf( type) > -1){
            return 2;
        }else if( this._resistance.indexOf( type) > -1){
            return 0.5;
        }else{ return 1;}
    }
    addFaiblesse(...faiblesses: string[]){
        faiblesses.forEach( faiblesse => {
            this._faiblesse.push(faiblesse);
        });
    }
    addResistance(...resistances: string[]){
        resistances.forEach( resistance => {
            this._resistance.push(resistance);
        });
    }
    removeFaiblesse(...faiblesses: string[]){
        faiblesses.forEach( faiblesse => {
            let tmp = this._faiblesse.indexOf( faiblesse);
            if( tmp > -1)
                this._faiblesse.splice(tmp, 1);
        });
    }
    removeResistance(...resistances: string[]){
        resistances.forEach( resistance => {
            let tmp = this._resistance.indexOf( resistance);
            if( tmp > -1)
                this._resistance.splice(tmp, 1);
        });
    }

    get KO(): boolean {
        return this._vitalite <= 0;
    }
    get nom(): string {
        return this._nom;
    }
    get type(): string {
        return this._type;
    }
    get niveau(): number {
        return this._niveau;
    }

}