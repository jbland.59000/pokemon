import Pokemon from "./pokemon";

export default class Pokeball {
    _pokemon?: Pokemon
    constructor(pokemon?: Pokemon){
        this._pokemon = pokemon;
    }

    capturer(pokemon: Pokemon): void{
        this._pokemon = pokemon;
    }

    get pokemon(): Pokemon | null {
        return this._pokemon || null;
    }
}