import Type from "../Interfaces/type";
import Pokemon from "./pokemon";

export default class Carapuce extends Pokemon {
    _niveau: number;
    constructor(nom: string, niveau?: number){
        super(nom, niveau || 1);
        this._type = 'Eau';
        this.addFaiblesse('Plante');
        this.addResistance('Feu')
    }
    monterNiveau(total?: number): void {
        this._niveau += total || 1;
        if( this._niveau > 100){
            console.error( 'votre pokemon est au niveau max');
            this._niveau = 100;
            return;
        }
        this._puissance = this._niveau /2 + 5;
        this._vitaliteMax = this._niveau * 2+5;
    }
}