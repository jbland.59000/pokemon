import Perso from "../Interfaces/perso";
import Pokeball from "./pokeball";
import Pokemon from "./pokemon";

export default class Dresseur implements Perso {
    _nom: string;
    pokeballs: Pokeball[];
    constructor(nom: string){
        this._nom = nom;
    }
    renommer(nom: string): void {
        this._nom = nom;
    }
    get nom(): string {
        return this._nom;
    }
    ajouterPokeball(pokeball: Pokeball){
        this.pokeballs.push(pokeball);
    }
    capturer(pokemon: Pokemon): boolean{
        this.pokeballs.forEach(pokeball => {
            if (pokeball.pokemon == null){
                pokeball.capturer(pokemon);
            }
        });
        return false;
    }
    get pokemons(): Pokemon[]{
        let tmp: Pokemon[] = [];
        this.pokeballs.forEach(pokeball => {
            if (pokeball.pokemon != null)
                tmp.push(pokeball.pokemon);
        });
        return tmp;
    }
}