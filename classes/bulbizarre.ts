import Pokemon from "./pokemon";

export default class Bulbizarre extends Pokemon {
    _niveau: number;
    constructor(nom: string, niveau?: number){
        super(nom, niveau || 1);
        this._type = 'Plante';
        this.addFaiblesse('Feu');
        this.addResistance('Eau');
    }
    monterNiveau(total?: number): void {
        this._niveau += total || 1;
        if( this._niveau > 9999){
            console.error( 'votre pokemon est une foret amazonienne');
            this._niveau = 100;
            return;
        }
        this._puissance = this._niveau + 1;
        this._vitaliteMax = this._niveau * 99+5;
    }
}